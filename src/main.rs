use std::fs::OpenOptions;
use std::io::SeekFrom;
use std::io::Seek;
use std::io::Read;

fn main() {
    let mut file = OpenOptions::new().write(true).read(true).open("4096block200").unwrap();
    file.seek(SeekFrom::Start(0)).unwrap();
    let mut buffer: [u8; 9] = [0,0,0,0,0,0,0,0,0];
    let res = file.read(&mut buffer).unwrap();
    println!("{:?}",buffer);
}
